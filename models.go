package main

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

type UserLogin struct {
	email    string `json:"email"`
	password string `json:"password"`
}

// User Model
type User struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Type     string
	Email    string
	Password string
	Role     string
	Valid    int
	Remember string
}

// UserLoginResult Model
type UserLoginResult struct {
	ID       bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Type     string
	Email    string
	Role     string
	Valid    int
	Remember string
}

// AccessToken Model
type AccessToken struct {
	UserID    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Token     string
	ExpiresAt string
}

// Client Model
type Client struct {
	ClientKey    string
	ClientSecret string
}

type Page struct {
	XnavID        int    `json:"xnav_id" db:"user_id"`
	SectionID     int    `json:"section_id" db:"section_id"`
	XnavParent    int    `json:"xnav_parent" db:"xnav_parent"`
	XnavRoot      int    `json:"xnav_root" db:"xnav_root"`
	XnavType      int    `json:"xnav_type" db:"xnav_type"`
	NavRedirect   int    `json:"nav_redirect" db:"nav_redirect"`
	NavName       string `json:"nav_name" db:"nav_name"`
	NavName2      string `json:"nav_name2" db:"nav_name2"`
	NavNameServer string `json:"nav_name_server" db:"nav_name_server"`
	SiteAppname   string `json:"site_appname" db:"site_appname"`
	SiteName      string `json:"site_name" db:"site_name"`
	Language      string `json:"language" db:"language"`
	Url           string `json:"url" db:"url"`
	SectionName   string `json:"section_name" db:"section_name"`
	XnavOrder     int    `json:"xnav_order" db:"xnav_order"`
	Child         []Page
}

// UserProfile Model
type UserProfile struct {
	ID                    bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UserID                string
	FirstName             string
	LastName              string
	Address               string
	PaymentMethod         string
	SubscriptionStartDate time.Time
	SubscriptionEndDate   time.Time
	Role                  string
	RoleFunction          string
	AuthType              string
	Email                 string
}
