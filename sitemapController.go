package main

import (
	"database/sql"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/godror/godror"
)

func getLivePages(c *gin.Context) {
	db, err := sql.Open("godror", username+"/"+password+"@"+host+"/"+database)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error in connecting to the database."})
		return
	}
	defer db.Close()

	xsiteID := c.Param("xsiteID")

	dbQuery :=
		`SELECT
	XNAV.XNAV_ORDER,
	XNAV.XNAV_ID,
	XNAV.XNAV_PARENT,
	XNAV.XNAV_ROOT,
	XNAV.XNAV_TYPE,
	NAV.NAV_REDIRECT,
	NAV.NAV_NAME,
	NAV.NAV_NAME2,
	NAV.NAV_NAME_SERVER,
	FW_SITE.SITE_APPNAME,
	FW_SITE.SITE_NAME,
	(SELECT LANG_SHORT FROM FW_LANG WHERE LANG_ID = FW_XSITE.LANG_ID) LANGUAGE,
	(
		SELECT
			LISTAGG(TNAV.NAV_NAME_SERVER, '/') WITHIN GROUP (ORDER BY LEVEL DESC)
		FROM
			XNAV TXNAV INNER JOIN NAV TNAV ON TXNAV.XNAV_ID = TNAV.XNAV_ID AND TNAV.NAV_VERSIONLIVE = 1
			START WITH TXNAV.XNAV_ID = XNAV.XNAV_ID
			CONNECT BY PRIOR TXNAV.XNAV_PARENT = TXNAV.XNAV_ID
	) URL,
	COALESCE(SECTION.SECTION_ID, 0) as SECTION_ID,
	SECTION.SECTION_NAME
	FROM
	XNAV INNER JOIN NAV ON XNAV.XNAV_ID = NAV.XNAV_ID
	LEFT JOIN SECTION ON SECTION.SECTION_ID = XNAV.SECTION_ID
	INNER JOIN FW_XSITE ON FW_XSITE.XSITE_ID = XNAV.XSITE_ID
	INNER JOIN FW_SITE ON FW_SITE.SITE_ID = FW_XSITE.SITE_ID
	INNER JOIN FW_LANG ON FW_LANG.LANG_ID = FW_XSITE.LANG_ID
	WHERE
	XNAV.XSITE_ID =` + xsiteID + `AND
	NAV.NAV_VERSIONLIVE = 1 AND
	(
		SECTION.SECTION_ID IN (
			SELECT 
				SECTION_ID 
			FROM 
				XSECTION INNER JOIN FW_XSITE ON XSECTION.SITE_ID = FW_XSITE.SITE_ID 
			WHERE 
				XSITE_ID = ` + xsiteID + `
		) OR
		XNAV.SECTION_ID = 0
	)
	ORDER BY section_id, xnav_parent, xnav_type, xnav_order
	`
	rows, err := db.Query(dbQuery)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": "Error in executing the query."})
		return
	}
	defer rows.Close()

	var xnav_order, xnav_id, xnav_parent, xnav_root, xnav_type, nav_redirect, section_id int
	var nav_name, nav_name2, nav_name_server, site_appname, site_name, language, url, section_name string
	// var results []Page

	output := []Page{}
	for rows.Next() {
		page := Page{}
		err := rows.Scan(&xnav_order, &xnav_id, &xnav_parent, &xnav_root, &xnav_type, &nav_redirect,
			&nav_name, &nav_name2, &nav_name_server, &site_appname, &site_name, &language, &url,
			&section_id, &section_name)
		if err != nil {
			c.JSON(http.StatusBadRequest, err)
			return
		}

		page.XnavOrder = xnav_order
		page.XnavID = xnav_id
		page.XnavParent = xnav_parent
		page.XnavRoot = xnav_root
		page.XnavType = xnav_type
		page.NavRedirect = nav_redirect
		page.NavName = nav_name
		page.NavName2 = nav_name2
		page.NavNameServer = nav_name_server
		page.SiteAppname = site_appname
		page.SiteName = site_name
		page.Language = language
		page.Url = url
		page.SectionID = section_id
		if section_name == "" {
			page.SectionName = "Home"
		} else {
			page.SectionName = section_name
		}

		output = append(output, page)

	}

	// results := make(map[int][]Page)
	// for _, v := range output {
	// 	v.Child = getChildren(output, v.XnavID, 0)
	// 	results[v.XnavParent] = append(results[v.XnavParent], v)
	// }

	// finalResults := make(map[string][]Page)
	// for _, v := range results {
	// 	for _, v1 := range v {
	// 		finalResults[v1.SectionName] = append(finalResults[v1.SectionName], v1)
	// 	}
	// }

	// file, _ := json.MarshalIndent(finalResults, "", " ")
	// _ = ioutil.WriteFile("/Users/xrey/Desktop/test.json", file, 0644)
	printTree(output, 0, 0)
	c.JSON(http.StatusOK, gin.H{"message": "okay"})
	return
}

func getChildren(pages []Page, parentID int, depth int) []Page {
	// var maxDepth int = 1

	results := []Page{}
	for _, v := range pages {
		if v.XnavParent == parentID {
			// if depth <= maxDepth {
			v.Child = getChildren(pages, v.XnavID, depth+1)
			// }
			results = append(results, v)
		}
	}
	return results
}

func printTree(tbl []Page, parent int, depth int) map[int][]Page {
	filename := "/Users/xrey/Desktop/test.txt"
	f, _ := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)

	relations := make(map[int][]Page)
	for _, r := range tbl {
		if r.XnavParent == parent {
			for i := 0; i < depth; i++ {
				// fmt.Print("\t")
				f.WriteString("\t")
			}
			// fmt.Print(r.NavName, "\n\n")
			if r.XnavType == 1 {
				f.WriteString(r.NavName + "[" + r.SiteName + "/" + r.Url + "/" + "]")
			} else {
				f.WriteString(r.NavName + "[" + r.SiteName + "/" + r.Url + ".cfm" + "]")
			}
			f.WriteString("\n\n")

			printTree(tbl, r.XnavID, depth+1)
		}
	}
	f.Close()
	return relations
}
