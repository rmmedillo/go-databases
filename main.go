package main

import (
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
)

const (
	username = "pandaorg_core"
	password = "pandap0wer"
	host     = "127.0.0.1:1525"
	database = "cms03"
)

// "MONGODB CONFIDENTIALS"
const (
	MongoDBHosts   = "127.0.0.1:27017"
	AuthDatabase   = "goDB"
	AuthUserName   = "admin"
	AuthPassword   = "1234"
	TestDatabase   = "goDB"
	dateFormat     = "2006-01-02 15:04:05"
	dateOnlyFormat = "2006-01-02"
)

var mySigningKey = []byte("Base")

var session mgo.Session

var mongoUser = &mgo.DialInfo{
	Addrs:    []string{MongoDBHosts},
	Database: AuthDatabase,
	Username: AuthUserName,
	Password: AuthPassword,
}

func main() {
	router := gin.Default()

	router.Use(CORSMiddleware())
	router.POST("/api/v1/auth/login", loginHandler)
	router.POST("/api/v1/auth/register", userRegistration)

	v1 := router.Group("/api/v1")
	// v1.Use(BeforeMiddleware, TokenMiddleware)
	// {
	v1.GET("/live-pages/:xsiteID", getLivePages)
	// }
	router.Run(":9009")

}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST,HEAD,PATCH, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
