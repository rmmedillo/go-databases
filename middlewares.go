package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func BeforeMiddleware(c *gin.Context) {
	session, serr := mgo.DialWithInfo(mongoUser)
	if serr != nil {
		panic(serr)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)

	dbClients := session.DB(AuthDatabase).C("api_clients")
	client := Client{}
	secret := c.Query("client_secret")
	key := c.Query("client_key")

	err := dbClients.Find(bson.M{"client_secret": secret, "client_key": key}).One(&client)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": "Unauthorized"})
		return
	}
	c.Next()
}

// TokenMiddleware : to check if token is valid
func TokenMiddleware(c *gin.Context) {
	jwtString := c.GetHeader("Authorization")
	token, err := jwt.Parse(jwtString, keyFn)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": "Invalid Token."})
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	nowDate := time.Now().Format(dateFormat)
	var tokenDate = claims["expires_at"].(string)
	cnvTokenDate, _ := time.Parse(dateFormat, tokenDate)
	cnvNowDate, _ := time.Parse(dateFormat, nowDate)
	isTokenExpired := cnvNowDate.After(cnvTokenDate)

	if err == nil && ok && token.Valid {
		if isTokenExpired {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": "Token Already Expired."})
			return
		}
		c.Next()
	} else {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"msg": "Invalid Token."})
		return
	}
}

func keyFn(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	}
	return []byte("Base"), nil
}
