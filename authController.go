package main

import (
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ## LOGIN ##
func loginHandler(c *gin.Context) {
	// buf := make([]byte, 1024)
	// num, _ := c.Request.Body.Read(buf)
	// reqBody := string(buf[0:num])
	userLogin := UserLogin{}
	c.Bind(&userLogin)

	session, merr := mgo.DialWithInfo(mongoUser)
	if merr != nil {
		panic(merr)
	}
	defer session.Close()
	session.SetMode(mgo.Monotonic, true)
	// "tables"
	m := session.DB(AuthDatabase).C("users")
	t := session.DB(AuthDatabase).C("access_tokens")
	tblUserProfile := session.DB(AuthDatabase).C("user_profile")

	var user User
	var userProfile UserProfile

	_pass := []byte(c.PostForm("password"))
	err := m.Find(bson.M{"email": c.PostForm("email")}).One(&user)
	tblUserProfile.Find(bson.M{"userid": user.ID.Hex()}).One(&userProfile)

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"msg": c.PostForm("email")})
		return
	}
	userProfile.Email = c.PostForm("email")

	checkPassword := bcrypt.CompareHashAndPassword([]byte(user.Password), _pass)
	if checkPassword != nil {
		c.JSON(http.StatusForbidden, gin.H{"msg": "Incorrect Password."})
		return
	}

	// # CREATE CLAIMS #
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["role"] = user.Role
	claims["id"] = user.ID
	claims["email"] = c.PostForm("email")
	expiryDate := time.Now().Add(time.Hour * 16).Format(dateFormat) // add 16 hours
	claims["expires_at"] = expiryDate

	tokenString, _ := token.SignedString(mySigningKey)
	c.JSON(http.StatusCreated, gin.H{
		"result":       "success",
		"access_token": tokenString,
		"expired_date": expiryDate,
		"user_profile": userProfile,
	})

	accessToken := AccessToken{
		UserID:    user.ID,
		Token:     tokenString,
		ExpiresAt: expiryDate,
	}

	t.Insert(&accessToken)
	return
}

func userRegistration(c *gin.Context) {
	session, err := mgo.DialWithInfo(mongoUser)
	if err != nil {
		panic(err)
	}
	defer session.Close()
	//Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	m := session.DB(AuthDatabase).C("users")
	tblUserProfile := session.DB(AuthDatabase).C("user_profile")

	password := []byte(c.PostForm("password"))
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	user := User{
		ID:       bson.NewObjectId(),
		Email:    c.PostForm("email"),
		Type:     c.PostForm("auth_type"),
		Password: string(hashedPassword),
		Role:     c.PostForm("role"),
		Valid:    1,
	}

	subsc_startdate, _ := time.Parse(dateOnlyFormat, c.PostForm("subscription_startdate"))
	subsc_enddate, _ := time.Parse(dateOnlyFormat, c.PostForm("subscription_enddate"))

	insertUserProfile := UserProfile{
		ID:                    bson.NewObjectId(),
		UserID:                user.ID.Hex(),
		FirstName:             c.PostForm("first_name"),
		LastName:              c.PostForm("last_name"),
		Address:               c.PostForm("address"),
		PaymentMethod:         c.PostForm("payment_method"),
		SubscriptionStartDate: subsc_startdate,
		SubscriptionEndDate:   subsc_enddate,
		Role:                  c.PostForm("role"),
		RoleFunction:          c.PostForm("role_function"),
		AuthType:              c.PostForm("auth_type"),
	}

	m.Insert(&user)
	tblUserProfile.Insert(&insertUserProfile)

	c.JSON(http.StatusCreated, gin.H{
		"msg":        "Successfully Created.",
		"resourceId": insertUserProfile.ID,
	})
}
